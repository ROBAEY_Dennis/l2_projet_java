package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;

import java.net.Socket;
import java.util.*;

public class BattleGame extends CardGame {
    public BattleGame(String name, Integer maxPlayers, String filename) {
        super(name,maxPlayers,filename);
    }

    /*public Player run(Socket socket) { (bonus du TP1)
        Collections.shuffle(cards);
        Integer nbPlayer = players.size();

        for (int i=0;i<cards.size();i++) { //on distribue à chaque joueur successivement une carte
            players.get(i%nbPlayer).addComponent(cards.get(i));
        }

        for (Player player : players) {
            player.canPlay(true);
        }

        Integer maxScoreOfThisRound;
        Player winnerOfThisRound = null;

        while(!end()) {
            numberOfRounds++;
            displayState();

            if (numberOfRounds%10==0) { //on mélange les cartes des joueurs tous les 10 tours
                for (Player player : players) {
                    player.shuffleHand();
                }
            }

            System.out.println("Round : " + numberOfRounds);

            maxScoreOfThisRound=0;

            Map<Player, Card> playedCard = new HashMap<>();
            for (Player player : players) { //on ajoute dans le hashmap les joueurs(avec leurs cartes)qui peuvent jouer
                if (player.isPlaying()) {
                    Card card = (Card)player.play() ;
                    if (card.getValue()>maxScoreOfThisRound) { //on garde le Player qui a eu la plus grosse valeur et la plus haute valeur( on réinitialise maxScoreOfThisRound=0 à chaque tour donc on rentrera forcément dans le if)
                        maxScoreOfThisRound= card.getValue();
                        winnerOfThisRound=player;
                    }

                    if (player.getComponents().size()==0) {
                        player.canPlay(false);
                    }
                    card.setHidden(false);
                    playedCard.put(player,card) ;

                    System.out.println(player.getName() + " has played " + card.getName()) ;
                }
            }

            ArrayList<Player> playersDraw=new ArrayList<>();

            for (Map.Entry<Player,Card> entry : playedCard.entrySet()) { //parmis les joueurs qui ont joués
                Player player = entry.getKey();
                Card card = entry.getValue();

                if (card.getValue().equals(maxScoreOfThisRound)) { // si il y a égalité
                    playersDraw.add(player); //on ajoute le(s) joueur(s) qui ont la même plus haute valeur, si playersDraw a une taille de 1, c'est juste le gagnant donc il n'y a pas d'égalité
                }
            }

            if (playersDraw.size()>1) { // si il y a eu une égalité : on désigne le gagnant par une autre bataille (si il n'y a qu'un joueur alors c'est le gagnant donc il n'y a pas d'égalité
                System.out.println("----------------- DRAW --------------------");
                ArrayList<Card> cardsPlayedInDraw = new ArrayList<>(); //on distribuera les cartes jouées au gagnant

                boolean jouerFaceCache=false;

                while(playersDraw.size()>1) { //tant qu'il y a égalité, on boucle (on clear playersDraw avant de jouer la bataille, où ajoutera les éventuelles joueurs à égalité)
                    jouerFaceCache=!jouerFaceCache; //si il y a une autre égalite : on joue une carte face caché, puis a lieu une autre bataille

                    ArrayList<Player> cardLessPlayers = new ArrayList<>();
                    Map<Player, Card> playedCard2 = new HashMap<>();

                    maxScoreOfThisRound=0;

                    for (Player player : playersDraw) { //on ajoute dans le hashmap les joueurs avec la carte qu'ils vont jouer
                        if (player.isPlaying()) { //si le joueur a encore des cartes
                            Card card = (Card)player.play() ;
                            cardsPlayedInDraw.add(card);

                            if (player.getComponents().size()==0) {
                                player.canPlay(false);
                            }
                            if (jouerFaceCache) {
                                card.setHidden(true);
                            } else {
                                if (card.getValue()>maxScoreOfThisRound)  {
                                    winnerOfThisRound=player;
                                    maxScoreOfThisRound=card.getValue();
                                }
                                card.setHidden(false);
                                playedCard2.put(player,card) ;
                                System.out.println("(draw)" + player.getName() + " has played " + card.getName()) ;
                            }
                        } else { //il n'a plus de carte :
                            cardLessPlayers.add(player);
                        }
                    }

                    boolean playersInDrawHaveNoCards=true;

                    if (cardLessPlayers.size()!=0) { //si des joueurs n'ont plus de carte et n'ont pas pu jouer
                        for (int i=0;i<cardLessPlayers.size();i++) {
                            playersInDrawHaveNoCards=true;
                            for (Player player : playersDraw) {
                                if (player.getComponents().size()!=0) {
                                    playersInDrawHaveNoCards=false;
                                    cardLessPlayers.get(i).addComponent(player.play());
                                    break;
                                }
                            }

                            if (playersInDrawHaveNoCards) { //si les joueurs à égalité n'ont plus de carte : alors c'est le joueur ayant le plus de carte (non présent lors de l'égalité) qui fournira les cartes
                                Player playerWithTheMostCards=null;
                                Integer maxCards=0;

                                for (Player player : players) {
                                    if (!playersDraw.contains(player)) { //si le joueur n'était pas dans l'égalité
                                        if (player.getComponents().size()>maxCards) {
                                            playerWithTheMostCards= player;
                                            maxCards=player.getComponents().size();
                                        }
                                    }
                                }
                                if (playerWithTheMostCards==null) { //les joueurs n'ont plus de carte
                                    System.out.println("Les joueurs n'ont plus de carte");
                                    return null;
                                }
                                cardLessPlayers.get(i).addComponent(playerWithTheMostCards.play());
                            }
                        }
                    }

                    for (Player player : cardLessPlayers) { //on fait jouer les joueurs qui n'avaient plus de carte (il n'auront plus de carte après avoir joué, donc on ne modifie pas playing qui était déjà à faux)
                        Card card = (Card)player.play() ;
                        if (card.getValue()>maxScoreOfThisRound)  {
                            winnerOfThisRound=player;
                            maxScoreOfThisRound=card.getValue();
                        }
                        cardsPlayedInDraw.add(card);

                        if (jouerFaceCache) {
                            card.setHidden(true);
                        } else {
                            card.setHidden(false);
                            playedCard2.put(player,card) ;
                            System.out.println("(draw)" + player.getName() + " has played " + card.getName()) ;
                        }
                    }

                    if (!jouerFaceCache) { //si il fallait jouer une carte face cachée, alors il n'y a pas de bataille

                        playersDraw.clear(); //sinon, on clear vide playersDraw : si il y a une autre égalité, on remplira le même tableau

                        for (Map.Entry<Player,Card> entry : playedCard2.entrySet()) { //parmis les joueurs qui ont joués, on vérifie si il y a une égalité
                            Player player = entry.getKey();
                            Card card = entry.getValue();

                            if (card.getValue().equals(maxScoreOfThisRound)) { // si il y a égalité
                                playersDraw.add(player);
                            }
                        }
                    }
                }

                for (Card card : cardsPlayedInDraw) { //on distribue toute les cartes jouées au gagnant
                    winnerOfThisRound.addComponent(card);
                }
            }

            for (Map.Entry<Player,Card> entry : playedCard.entrySet()) { //on ajoute au gagnant les cartes des joueurs qui ont été retirées (y compris la carte du gagnant, qui a aussi été retirée via player.play())
                winnerOfThisRound.addComponent(entry.getValue());
                if (entry.getKey().getComponents().size()==0) { //si le joueur n'a plus de carte il ne peut plus jouer
                    entry.getKey().canPlay(false);
                } else {
                    entry.getKey().canPlay(true);
                }
            }
        }

        displayState();

        return winnerOfThisRound; //on retourne le gagnant (qui sera forcément le gagnant du dernier tour)
    }*/
}
