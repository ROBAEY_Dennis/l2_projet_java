package ulco.cardGame.common.games;

import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.BoardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public abstract class BoardGame implements Game {
    protected String name;
    protected Integer maxPlayers;
    protected List<BoardPlayer> players;
    protected boolean endGame, started;
    protected Board board;

    public BoardGame(String name, Integer maxPlayers, String filename) {
        this.maxPlayers=maxPlayers;
        this.name=name;
        initialize(filename);
    }

    public boolean addPlayer(Socket socket, Player player) {
        if (players.size()>=maxPlayers) {
            try {
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject("Nombre de player maximum atteint");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        for (Player p : players) {
            if (p.getName().equals(player.getName())) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject("Pseudo déjà utilisé");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }
        }

        players.add((BoardPlayer)player);
        System.out.println("Player added into players game list");
        return true;
    }

    public void removePlayer(Player player){
        players.remove(player);
    }

    public void removePlayers(){
        players.clear();
    }

    public Player getCurrentPlayer(String name) {
        for (Player p : players) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    public void displayState() {
        System.out.println("-------------------------------------------");
        System.out.println("--------------- Game State ----------------");
        System.out.println("-------------------------------------------");
        for (Player p : players) {
            System.out.println(p.toString());
        }
        System.out.println("-------------------------------------------");
    }

    public boolean isStarted() {
        if (started || players.size()>=2) {
            return true;
        }
        return false;
    }

    public Integer maxNumberOfPlayers() {
        return maxPlayers;
    }

    public List<BoardPlayer> getPlayers() {
        return players;
    }

    public Board getBoard(){
        return board;
    }
}