package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.BoardPlayer;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class CardGame extends BoardGame {

    protected List<Card> cards;
    protected Integer numberOfRounds=0;


    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name,maxPlayers,filename);
        players = new ArrayList<BoardPlayer>();
        board=new CardBoard();
    }

    public void initialize(String filename) {
        cards = new ArrayList<Card>();

        try {
            File cardFile = new File ( filename ) ;
            Scanner myReader = new Scanner ( cardFile ) ;
            while ( myReader . hasNextLine () ) {
                String data=myReader.nextLine () ;
                String[] cardData = data.split(";"); //le premier élément sera le nom, le deuxième la valeur (qu'il faudra convertir en int)
                cards.add(new Card(cardData[0],Integer.parseInt(cardData[1]), true));
            }
            myReader.close () ;

        } catch ( FileNotFoundException e ) {
            System.out.println("An error occurred .") ;
            e.printStackTrace () ;
        }

    }

    public Player run(Map<Player, Socket>map) {
        Collections.shuffle(cards);
        Integer nbPlayer = players.size();

        for (int i=0;i<cards.size();i++) { //on distribue à chaque joueur successivement une carte
            players.get(i%nbPlayer).addComponent(cards.get(i));
        }

        for (Player player : players) {
            player.canPlay(true);
            Socket socket=map.get(player);
            try {
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Integer maxScoreOfThisRound;
        Player winnerOfThisRound = null;

        ArrayList<Player> playersDraw=new ArrayList<>(); //le tableau qui contiendra les joueurs à égalité

        try {
            while(!end()) {
                numberOfRounds++;
                displayState();
                playersDraw.clear();
                board.clear();

                if (numberOfRounds%10==0) { //on mélange les cartes des joueurs tous les 10 tours
                    for (Player player : players) {
                        player.shuffleHand();
                    }
                }

                System.out.println("Round : " + numberOfRounds);

                for (Player p : players) { //on envoie à tout les joueurs le numéro du round
                    Socket socket=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject("Round : " + numberOfRounds);
                }

                maxScoreOfThisRound=0;

                Map<Player,Card> playedCard = new HashMap<>();
                for (Player player : players) { //on ajoute dans le hashmap les joueurs(avec leurs cartes jouées)
                    if (player.isPlaying()) { //si ils peuvent joués (ils ont au moins une carte)
                        Socket socket=map.get(player);
                        for (Player otherPlayer : players) { //boucle pour indiquer aux joueurs si ils doivent jouer, ou attendre qu'un autre joueur joue
                            Socket socket2=map.get(otherPlayer);
                            if (player==otherPlayer) {
                                ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                                oos.writeObject("[" + player.getName() + "], you have to play...");
                            } else {
                                ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                                oos.writeObject("Waiting for " + player.getName() + " to play...");
                            }

                        }

                        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                        Card card = (Card) ois.readObject();

                        //retirer la carte de la main du joueur qui a joué
                        for (Component c : player.getSpecificComponents(Card.class)) {
                            if (card.getId().equals(c.getId())) {
                                player.removeComponent(c);
                                break;
                            }
                        }

                        for (Player p : players) { //on affiche à tout les joueurs la carte qui vient d'être jouée
                            Socket socket2=map.get(p);
                            ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                            oos.writeObject(player.getName() + " has played " + card.getName());
                        }

                        card.setPlayer(player);
                        board.addComponent(card);

                        if (card.getValue()>maxScoreOfThisRound) { //on garde le Player qui a eu la plus grosse valeur( on réinitialise maxScoreOfThisRound=0 à chaque tour donc on rentrera forcément dans le if)
                            maxScoreOfThisRound= card.getValue();
                            winnerOfThisRound=player;
                        }
                        card.setHidden(false);
                        playedCard.put(player,card) ;

                        System.out.println(player.getName() + " has played " + card.getName()) ;
                    }
                }

                for (Player p : players) { //on envoie à tout les joueurs le nouvel état du board
                    Socket socket=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(board);
                }

                for (Map.Entry<Player,Card> entry : playedCard.entrySet()) { //parmis les joueurs qui ont joués
                    Player player = entry.getKey();
                    Card card = entry.getValue();

                    if (card.getValue().equals(maxScoreOfThisRound)) { // si il y a égalité
                        playersDraw.add(player);
                    }
                }

                if (playersDraw.size()>1) { // si il y a eu une égalité : on désigne le gagnant aléatoirement (si playersDraw a une taille de 1, alors c'est le gagnant donc il n'y a pas d'égalité)
                    Random random=new Random();
                    winnerOfThisRound = playersDraw.get(random.nextInt(playersDraw.size())); //on choisi, parmis les joueurs à égalité le gagnant aléatoirement à partir de son index
                }

                for (Map.Entry<Player,Card> entry : playedCard.entrySet()) { //on ajoute au gagnant les cartes des joueurs qui ont été retirées (y compris la carte du gagnant, qui a aussi été retirée via player.play())
                    winnerOfThisRound.addComponent(entry.getValue());
                    if (entry.getKey().getComponents().size()==0) { //si le joueur n'a plus de carte il ne peut plus jouer
                        entry.getKey().canPlay(false);
                    }
                }

                board.displayState();

                for (Player p : players) {
                    Socket socket2=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                    oos.writeObject("Player : " + winnerOfThisRound.getName()  + " won the round with Card " + playedCard.get(winnerOfThisRound).toString());
                }

                for (Player p : players) { //on envoie à tout les joueurs le nouvel état du jeu (avec les cartes retirés des joueurs)
                    Socket socket=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(this);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }


        board.clear();

        displayState();


        return winnerOfThisRound; //on retourne le gagnant (qui sera forcément le gagnant du dernier tour)
    }

    public boolean end() {
        for (Player p : getPlayers()) {
            if (p.getComponents().size()==52) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return this.name;
    }
}
