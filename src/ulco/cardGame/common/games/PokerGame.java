package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.BoardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class PokerGame extends BoardGame{
    private List<Card>cards;
    private List<Coin>coins;
    Integer maxRounds;
    Integer numberOfRounds;

    public PokerGame(String name, Integer maxPlayers, String filename, Integer maxRounds) {
        super(name,maxPlayers,filename);
        players = new ArrayList<BoardPlayer>();
        board=new PokerBoard();
        this.maxRounds=maxRounds;
    }

    public void initialize(String filename) {
        cards = new ArrayList<>();
        coins = new ArrayList<>();

        try {
            File cardFile = new File ( filename ) ;
            Scanner myReader = new Scanner ( cardFile ) ;
            while ( myReader . hasNextLine () ) {
                String data=myReader.nextLine () ;
                String[] Data = data.split(";"); //le premier élément indique si c'est une carte ou un coin,
                // le deuxièment élément sera le nom(ou la couleur), le troisième la valeur (qu'il faudra convertir en int)
                if (Data[0].equals("Card")) {
                    cards.add(new Card(Data[1],Integer.parseInt(Data[2]), true));
                } else {
                    coins.add(new Coin(Data[1],Integer.parseInt(Data[2])));
                }

            }
            myReader.close () ;

        } catch ( FileNotFoundException e ) {
            System.out.println("An error occurred .") ;
            e.printStackTrace () ;
        }
    }

    public Player run(Map<Player, Socket>map) {
        numberOfRounds=0;
        Collections.shuffle(cards);
        Collections.shuffle(coins);

        //distribution des jetons à tout les joueurs
        for (Coin c : coins) {
            for (Player p : players) {
                p.addComponent(c);
            }
        }

        ArrayList<Player> playersInDraw=new ArrayList<>(); //le tableau qui contiendra les joueurs à égalité
        int NbSkip=0;
        try {
            while (!end()) {
                numberOfRounds++;
                Collections.shuffle(cards);
                playersInDraw.clear();
                board.clear();
                NbSkip = 0;

                for (int i = 0; i < 3; i++) { //distribution des cartes à chacun des joueurs jusqu'à ce qu'ils en possèdent 3
                    for (Player p : players) {
                        p.canPlay(true); //le joueur, si il a choisi de passer précédent tour peut rejouer pour le nouveau tour
                        Card card = cards.get(0);
                        card.setHidden(false);
                        p.addComponent(card);
                        cards.remove(card);
                    }
                }

                for (Card card : cards) { //le reste des cartes est face cachée
                    card.setHidden(true);
                }

                for (Player p : players) { //on envoie à tout les joueurs le nouvel état du jeu
                    Socket socket=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(this);
                }

                for (Player p : players) { //première mise de jeton
                    if (NbSkip == players.size() - 1) { //si tous les joueurs sauf 1 ont passé leur tour, alors c'est lui le gagnant
                        break;
                    }

                    Socket socket = map.get(p);
                    for (Player otherPlayer : players) { //boucle pour indiquer aux joueurs si ils doivent jouer, ou attendre qu'un autre joueur joue
                        Socket socket2 = map.get(otherPlayer);
                        if (p == otherPlayer) {
                            ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                            oos.writeObject("[" + p.getName() + "], you have to play...\n"+ p.handToString());
                        } else {
                            ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                            oos.writeObject("Waiting for " + p.getName() + " to play...");
                        }

                    }

                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    Coin coin = (Coin) ois.readObject();

                    if (coin!=null) { //si le joueur n'a pas passé son tour
                        //retirer le jeton de la main du joueur qui a joué
                        for (Component c : p.getSpecificComponents(Coin.class)) {
                            if (coin.getId().equals(c.getId())) {
                                p.removeComponent(c);
                                break;
                            }
                        }
                    }

                    if (coin == null) { //si coin est null, le joueur a choisi de passer son tour
                        p.canPlay(false);
                        NbSkip++;
                        for (Player player : players) { //on envoie à tout les joueurs ce que le joueur a passé son tour
                            Socket socket2=map.get(player);
                            ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                            oos.writeObject(p.getName() + " skipped");
                        }
                    } else {
                        board.addComponent(coin);
                        for (Player player : players) { //on envoie à tout les joueurs ce que le joueur a misé
                            Socket socket2=map.get(player);
                            ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                            oos.writeObject(p.getName() + " placed " + coin.getName());
                        }
                    }
                }

                for (int i = 0; i < 3; i++) { //ajout des 3 nouvelles cartes sur le board puis on l'affiche
                    Card card = cards.get(0);
                    card.setHidden(false);
                    board.addComponent(card);
                    cards.remove(card);
                }


                board.displayState();
                for (Player p : players) { //on envoie à tout les joueurs le nouvel état du board
                    Socket socket=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(board);
                }

                for (Player p : players) { //on envoie à tout les joueurs le nouvel état du jeu (avec les cartes, jetons retirés)
                    Socket socket=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(this);
                }

                for (Player p : players) { //deuxième mise de jeton
                    if (NbSkip == players.size() - 1) { //si tous les joueurs sauf 1 ont passé leur tour, alors c'est lui le gagnant
                        break;
                    }

                    if (p.isPlaying()) { //si le joueur n'a pas passé son tour
                        Socket socket = map.get(p);

                        for (Player otherPlayer : players) { //boucle pour indiquer aux joueurs si ils doivent jouer, ou attendre qu'un autre joueur joue
                            Socket socket2 = map.get(otherPlayer);
                            if (p == otherPlayer) {
                                ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                                oos.writeObject("[" + p.getName() + "], you have to play...\n"+ p.handToString());
                            } else {
                                ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                                oos.writeObject("Waiting for " + p.getName() + " to play...");
                            }

                        }

                        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                        Coin coin = (Coin) ois.readObject();

                        if (coin!=null) { //si le joueur n'a pas passé son tour
                            //retirer le jeton de la main du joueur qui a joué
                            for (Component c : p.getSpecificComponents(Coin.class)) {
                                if (coin.getId().equals(c.getId())) {
                                    p.removeComponent(c);
                                    break;
                                }
                            }
                        }

                        if (coin == null) { //si coin est null, le joueur a choisi de passer son tour
                            p.canPlay(false);
                            NbSkip++;
                            for (Player player : players) { //on envoie à tout les joueurs ce que le joueur a passé son tour
                                Socket socket2=map.get(player);
                                ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                                oos.writeObject(p.getName() + " skipped");
                            }
                        } else {
                            board.addComponent(coin);
                            for (Player player : players) { //on envoie à tout les joueurs ce que le joueur a misé
                                Socket socket2=map.get(player);
                                ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                                oos.writeObject(p.getName() + " placed " + coin.getName());
                            }
                        }
                    }
                }

                System.out.println("                ------------               ");

                int maxSameCardsOfThisRound = 0;
                Player winnerOfThisRound = null;
                Map<Player, Integer> maxSameCardsOfPlayers = new HashMap<>();  // pour chaque joueur qui ont joué : on garde leur plus grand nombre de cartes identiques et leur hauteur
                Map<Player, Integer> maxValueOfPlayers = new HashMap<>();

                for (Player p : players) { //pour chaque joueur, on regarde si il n'a pas passé son tour et si il ne l'a pas passé, on garde son plus grand nombre de cartes identiques (et leurs hauteurs)
                    if (p.isPlaying()) {
                        List<Component> cardsPlayerAndBoard = p.getSpecificComponents(Card.class);
                        cardsPlayerAndBoard.addAll(board.getSpecificComponents(Card.class)); //ajout dans cardsPlayerAndBoard les cartes du joueur, puis du board

                        List<Integer> valueCardsPlayerAndBoard = new ArrayList<>(); //ajout de chaque valeur de chaque carte dans une liste d'entier : ceci servira pour compter le nombre de carte de même valeur
                        for (Component c : cardsPlayerAndBoard) {
                            valueCardsPlayerAndBoard.add(c.getValue());
                        }

                        int maxSameCards = 0, maxValue = 0;

                        for (int i = 2; i <= 14; i++) { //on regarde la fréquence des cartes de même valeur  : on conserve le plus grand nombre de cartes identiques ayant la même valeur (entre le 2(valeur=2) et l'as(valeur=14))
                            if (Collections.frequency(valueCardsPlayerAndBoard, i) >= maxSameCards) { //si c'est égal, alors il y a autant de carte mais la valeur sera plus grande, donc on change
                                maxValue = i;
                                maxSameCards = Collections.frequency(valueCardsPlayerAndBoard, i);
                            }
                        }

                        System.out.println("Player " + p.getName() + " has " + maxSameCards + " card(s) of value " + maxValue);

                        for (Player player : players) { //affichage chez les joueurs
                            Socket socket2=map.get(player);
                            ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                            oos.writeObject("Player " + p.getName() + " has " + maxSameCards + " card(s) of value " + maxValue);
                        }

                        maxSameCardsOfPlayers.put(p, maxSameCards); // on stocke le joueur avec son plus grand nombre de cartes indentiques
                        maxValueOfPlayers.put(p, maxValue); // on stocke le joueur avec la valeur la plus élevé des ses cartes indentiques

                        if (maxSameCards > maxSameCardsOfThisRound) { //on garde le plus grand nombre de carte identique, qui servira si il y a égalité
                            maxSameCardsOfThisRound = maxSameCards;
                            winnerOfThisRound = p;
                        }
                    } else {
                        System.out.println("Player " + p.getName() + " skipped");
                        for (Player player : players) { //affichage chez les joueurs
                            Socket socket2=map.get(player);
                            ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                            oos.writeObject("Player " + p.getName() + " skipped");
                        }
                    }
                }
                System.out.println("                ------------               ");

                for (Player p : players) {
                    if (p.isPlaying()) { //si le joueur n'a pas passé son tour
                        if (maxSameCardsOfPlayers.get(p) == maxSameCardsOfThisRound) { //on ajoute dans playersInDraw les joueurs qui le même plus grand nombre de carte identique
                            playersInDraw.add(p);
                        }
                    }
                }

                if (playersInDraw.size() > 1) { //si playersInDraw contient 1 joueur : c'est le gagnant, si il en contient plus c'est les joueurs à égalité
                    //si il y a égalité, on regarde premièrement le joueur ayant les cartes avec la plus grande valeur/hauteur
                    System.out.println("égalite");
                    int maxValueInDraw = 0;

                    for (Player p : playersInDraw) { //boucle pour trouver la plus grande hauteur des cartes identiques
                        if (maxValueOfPlayers.get(p) > maxValueInDraw) {
                            maxValueInDraw = maxValueOfPlayers.get(p);
                            winnerOfThisRound = p;
                        }
                    }

                    ArrayList<Player> completeDraw = new ArrayList<>(); //Liste qui contiendra les joueurs qui ont la même hauteur/valeur des cartes identiques

                    for (Player p : playersInDraw) {
                        if (maxValueOfPlayers.get(p) == maxValueInDraw) {
                            completeDraw.add(p);
                        }
                    }

                    if (completeDraw.size() > 1) { //si au moins 2 joueurs ont le même nombre de carte identiques avec la même valeur/hauteur, on choisi aléatoirement
                        System.out.println("égalité complète");
                        Random random = new Random();
                        winnerOfThisRound = completeDraw.get(random.nextInt(completeDraw.size())); //on choisi, parmis les joueurs à égalité le gagnant aléatoirement à partir de son index
                    }
                }

                for (Component c : board.getSpecificComponents(Coin.class)) { //on distribue les jetons au gagnant
                    Coin coin = (Coin) c;
                    winnerOfThisRound.addComponent(coin);
                }

                String endRoundToString="Player " + winnerOfThisRound.getName() + " won the game with " + maxSameCardsOfThisRound + " same card(s) of value " + maxValueOfPlayers.get(winnerOfThisRound);
                endRoundToString+="\n                ------------               ";
                endRoundToString+="\nEnd of round n°" + numberOfRounds + "/" + maxRounds;

                System.out.println(endRoundToString);
                displayState();

                for (Player p : players) { //affichage chez les joueurs de la fin du round + gagnant
                    Socket socket2=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket2.getOutputStream());
                    oos.writeObject(endRoundToString);
                }

                for (Player p : players) { //on retire les cartes en main des joueurs pour le remettre dans le packet principal
                    List<Component> components = p.getSpecificComponents(Card.class);
                    for (Component component : components) {
                        cards.add((Card) component);
                    }
                    p.clearHand();
                }

                for (Player p : players) { //on envoie à tout les joueurs le nouvel état du jeu
                    Socket socket=map.get(p);
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(this);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        Integer maxScore=0;
        Player winner=null;

        for (Player p : players) { //recherche du gagnant : celui avec le plus gros score
            if (p.getScore()>maxScore) {
                winner=p;
                maxScore=p.getScore();
            }
        }

        return winner;
    }

    public boolean end() {
        if (numberOfRounds.equals(maxRounds)){
            return true;
        }
        return false;
    }

    public String toString() {
        return this.name;
    }
}
