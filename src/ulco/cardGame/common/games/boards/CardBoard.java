package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Player;

import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {
    private List<Card> cards;

    public CardBoard(){
        cards=new ArrayList<>();
    }

    public void clear() {
        cards.clear();
    }

    public void addComponent(Component component) {
        cards.add((Card)component);
    }

    public List<Component> getComponents() {
        return new ArrayList<>(cards);
    }

    public List<Component> getSpecificComponents(Class<? extends Component> classType) {
        return new ArrayList<>(cards);
    }

    public void displayState() {
        System.out.println("-------------- Board State ----------------");
        for (Card c : cards) {
            System.out.println(c.getPlayer().getName() + " a joué " + c.toString());
        }
    }
}
