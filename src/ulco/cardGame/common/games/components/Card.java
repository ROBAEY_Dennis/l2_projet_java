package ulco.cardGame.common.games.components;

import ulco.cardGame.common.interfaces.Player;

public class Card extends Component {
    private boolean hidden;

    public Card(String name, Integer value, boolean bool) {
        super(name,value);
        hidden=bool;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String toString() {
        String chaine;
        if (hidden) {
            chaine="(caché)";
        } else {
            chaine="(visible)";
        }

        return this.name + ", valeur : " + this.value + " " + chaine;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player=player;
    }
}
