package ulco.cardGame.common.players;

import ulco.cardGame.common.interfaces.Player;

public abstract class BoardPlayer implements Player {
    protected String name;
    protected boolean playing;
    protected Integer score;

    public BoardPlayer(String name) {
        this.name=name;
        score=0;
        playing=false;
    }

    public void canPlay(boolean playing)  {
        this.playing=playing;
    }

    public Integer getScore() {
        return this.score;
    }

    public String getName() {
        return name;
    }

    public boolean isPlaying() {
        return playing;
    }

    public String toString() {
        return "BoardPlayer {" + "name='" + name + '\'' + ", playing=" + playing + ", score=" + score + '}';
    }

}
