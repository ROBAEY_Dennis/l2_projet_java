package ulco.cardGame.common.players;


import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardPlayer extends BoardPlayer {

    private List<Card> cards;

    public CardPlayer(String name) {
        super(name);
        cards = new ArrayList<Card>();
    }

    public Integer getScore() {
        return this.score;
    }

    public void addComponent(Component component) {
        cards.add((Card)component);
        score++;
    }

    public void removeComponent(Component component) {
        cards.remove((Card)component);
        score--;
    }

    public void play(Socket socket) {
        Card c=cards.get(0);
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(c);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //removeComponent(c);
    }

    public List<Component> getComponents() {
        return new ArrayList<>(cards);
    }

    public List<Component> getSpecificComponents() {
        return new ArrayList<>(cards);
    }

    public void shuffleHand() {
        Collections.shuffle(cards);
    }

    public void clearHand() {
        cards.clear();
    }

    @Override
    public List<Component> getSpecificComponents(Class<? extends Component> classType) {
        return new ArrayList<>(cards);
    }

    public void displayHand() {
        System.out.println("-------------------------------------------");
        System.out.println("Hand of [" + this.getName() + "]");
        System.out.println("                ------------               ");

        for (Card c : cards) {
            System.out.println("Card: " + c.getName());
        }

        System.out.println("-------------------------------------------");
    }

    public String toString() {
        return "CardPlayer{" + "name='" + name + '\'' + ", playing=" + playing + ", score=" + score + '}';
    }

    public String handToString() { //utilise seulement chez pokerPlayer
        return null;
    }
}
