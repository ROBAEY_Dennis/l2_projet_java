package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PokerPlayer extends BoardPlayer {
    private List<Card>cards;
    private List<Coin>coins;

    public PokerPlayer(String name){
        super(name);
        cards = new ArrayList<Card>();
        coins = new ArrayList<Coin>();
    }

    public Integer getScore() {
        return this.score;
    }

    public void play(Socket socket){
        Scanner scanner = new Scanner(System.in);
        boolean correctInput =false;
        Coin coin=null;

        while (!correctInput) {
            System.out.println("[" + getName() + "] select a valid Coin to play (coin color) with a uppercase letter or 'skip' to skip");
            String input=scanner.nextLine();
            if (input.equals("skip")) {
                correctInput=true;
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream()); //on envoie "skip" au serveur si le joueur souhaite passer
                    oos.writeObject(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            for (Coin c : coins) {
                if (c.getName().equals(input)) {
                    correctInput=true;
                    coin=c;
                    try {
                        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream()); //on envoie le jeton joué au serveur
                        oos.writeObject(coin);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }

       // removeComponent(coin);
       // return coin;
    }

    public void addComponent(Component component){
        if (component instanceof Card) {
            cards.add((Card)component);
        } else {
            score+=component.getValue();
            coins.add((Coin) component);
        }
    }

    public void removeComponent(Component component){
        if (component instanceof Card){
            cards.remove((Card)component);
        } else {
            score-=component.getValue();
            coins.remove((Coin)component);
        }
    }

    public List<Component> getComponents() {
        List<Component> list = new ArrayList<Component>();
        list.addAll(cards);
        list.addAll(coins);

        return list;
    }

    public List<Component> getSpecificComponents(Class<? extends Component>classType){
        if (classType == Card.class) {
            return new ArrayList<>(cards);
        } else {
            return new ArrayList<>(coins);
        }
    }

    public void shuffleHand() {
        Collections.shuffle(cards);
    }

    public void clearHand(){
        cards.clear();
    }

    public void displayHand(){
        System.out.println("-------------------------------------------");
        System.out.println("Hand of [" + this.getName() + "]");
        System.out.println("                ------------               ");

        for (Card c : cards) {
            System.out.println("Card: " + c.getName());
        }
        System.out.println("                ------------               ");

        List<String> listColor = new ArrayList<>();
        int sum=0;
        for (Coin c : coins) {
            listColor.add(c.getName());
            sum+=c.getValue();
        }

        System.out.println("- Coin red x " + Collections.frequency(listColor,"Red"));
        System.out.println("- Coin blue x " + Collections.frequency(listColor,"Blue"));
        System.out.println("- Coin black x " + Collections.frequency(listColor,"Black"));

        System.out.println("Your Coins sum is about: " + sum);
        System.out.println("-------------------------------------------");
    }

    public String handToString(){
        String hand="----------------Your hand------------------";

        for (Card c : cards) {
            hand+="\nCard: " + c.getName();
        }
        hand+="\n                ------------               ";

        List<String> listColor = new ArrayList<>();
        int sum=0;
        for (Coin c : coins) {
            listColor.add(c.getName());
            sum+=c.getValue();
        }

        hand+="\n- Coin red x " + Collections.frequency(listColor,"Red");
        hand+="\n- Coin blue x " + Collections.frequency(listColor,"Blue");
        hand+="\n- Coin black x " + Collections.frequency(listColor,"Black");

        hand+="\nYour Coins sum is about: " + sum;
        hand+="\n-------------------------------------------";

        return hand;
    }

    public String toString() {
        return "PokerPlayer{" + "name='" + name + '\'' + ", score=" + score +'}';
    }
}
